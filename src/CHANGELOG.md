# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Develop Theme 1
----------

v.1.1.2.1
----------
* Update Registration @dk
* Add Cookies Popup @dk
* GDPR settings @dk

v.1.1.1.0[stable]
----------
* Endpoint GDPR @dk

v.0.4.6.7[stable]
----------
* Task #24997645: About Us page BE @dk
* Task #24997646: Case listing BE @dk
* Task #24997650: Case Single BE @dk
* Task #24997671: TnC/Privacy BE @dk
* Task #24997666: 404 page BE @dk
* Task #25047655: Contact page BE @dk

v.0.3.5.6
----------
* Task #24961671: FAQ page BE @dk
* Task #24961691: Borrow BE @dk

v.0.3.4.5
----------
* Task #24893556: Blog Listing BE @dk
* Task #24893560: Category Listing BE @dk
* Task #24927815: Article single BE @dk
* Task #24940882: How platform works BE @dk
* Task #24952023: How investment works BE @dk

v.0.2.3.4
----------
* Task #24625888: Start Section BE @dk
* Task #24625901: Articles Section BE @dk
* Task #24625902: Cards Section BE @dk
* Task #24625903: FAQ Section BE @dk
* Task #24626601: Default Page BE @dk

v.0.2.2.3
----------
* Task #24625887: Form Section BE @dk

v.0.2.1.2
----------
* Task #24625904: Footer BE @dk
* Task #24625876: Hero Section BE @dk
* Task #24625877: Open opportunities BE @dk
* Task #24625878: Features section BE @dk
* Task #24625879: About Us BE @dk
* Task #24625875: Navigation BE @dk
* Task #24625881: How It Works BE @dk
* Task #24625882: Partners BE @dk
* Task #24625884: Testimonials BE @dk
* Task #24625886: Info/Statistics section BE @dk

* Task #24625857: Layout and styles of navigation @op
* Task #24625872: Layout and styles of footer @op
* Task #24625858: Layout and styles of Hero Section @op
* Task #24625859: Layout and styles of Open Opportunities @op
* Task #24625860: Layout and styles of Features section+CTA @op
* Task #24625861: Layout and styles of About Us section @op
* Task #24625862: Layout and styles of How it work section @op
* Task #24625863: Layout and styles of Partners section @op
* Task #24625864: Layout and styles of Testimonials Module @op
* Task #24625865: Layout and styles of Info section @op

v.0.1.0.1
----------

* Task #24617007: Theme 1. Project setup @op

==========================


Develop Theme 2
----------

v0.5.0.1
----------
* Task #25184391: Speed optimization (implemented lazyload) FE @vp
* Task #25184366: Access denied page FE @vp
* Task #25171897: Login popup & registration page FE @vp

v0.4.5.5
----------
* Task #25054701: About Us BE @dk
* Task #25054707: Platform BE @dk
* Task #25054710: Investment BE @dk
* Task #25054712: Borrow BE @dk
* Task #25095877: FAQ BE @dk
* Task #25095878: TnC/Privacy BE @dk
* Task #25095879: 404 BE @dk
* Task #25095882: Contact BE @dk
* Task #25095975: Case Listing BE @dk
* Task #25095977: Case Single BE @dk

v0.3.4.4[rc]
----------
* Task #24907269: Blog Listing BE @dk
* Task #24912268: Category Listing BE @dk
* Task #24919974: Single Article BE @dk

v0.3.3.3[stable]
----------
* Task #24626587: Articles List BE @dk
* Task #24626999: Default Page BE @dk
* Task #24880820: Featured Articles BE @dk
* Task #24626999: Default page BE @dk

v0.3.2.2
----------
* Task #24625945: Open Opportunities BE @dk
* Task #24626584: FAQ BE @dk
* Task #24626585: Testimonials Module BE @dk

v0.3.1.1
----------
* Task #24625928: Hero BE @dk
* Task #24625929: About Us BE @dk
* Task #24625931: Cards + CTA BE @dk
* Task #24625937: How It Works BE @dk
* Task #24625939: Features BE @dk
* Task #24625940: Contact Form BE @dk
* Task #24625943: Users Flow BE @dk

v0.2.0.1
----------

* Task #24625906: Header  @vt
* Task #24625909: Footer  @vt
* Task #24625910: Hero  @vt
* Task #24625912: Cards + CTA  @vt
* Task #24625911: About us  @vt
* Task #24625914: Features  @vt
* Task #24625907: Navigation  @vt
* Task #24625917: Open Opportunities  @vt
* Task #24625913: How it works  @vt
* Task #24625916: Users flow  @vt
* Task #24625921: Articles list  @vt
* Task #24625920: Testimonials Module  @vt
* Task #24625915: Contact form  @vt
* Task #24625919: FAQ  @vt

v0.1.0.1
----------

* Task #24617015: Project review and documents creation (set up project)  @vt


Develop Theme 2
----------

v0.5.2.2
----------

* Task #25209910: How investment works @dk
* Task #25209912: How borrow works @dk
* Task #25209913: FAQ @dk
* Task #25209914: About us @dk
* Task #25209916: Case Listing @dk
* Task #25209917: Sinle case study @dk
* Task #25210334: Terms&Privacy policy&404 @dk
* Task #25210370: Contact page @dk

v0.5.1.1[stable]
----------

* Task #25146713: Home Page @dk
* Task #25198300: Blog Listing @dk
* Task #25198304: Category listing @dk
* Task #25198306: Single Article @dk
* Task #25209909: How Platform Work @dk

v0.4.0.1[stable]

----------

* Task #24997611: Contact us page @vp
* Task #24997608: 404 page @vp
* Task #24997607: T'n'C, Privacy policy @vp
* Task #24997606: Single case study @vp
* Task #24997605: Case study listing @vp
* Task #24997601: About us @vp
* Task #24997600: FAQ @vp
* Task #24997598: How borrow works @vp


v0.3.0.1[stable]

----------

* Task #24962373: How investment works @vp
* Task #24937820: How platform works @vp
* Task #24920612: Single article @vp
* Task #24910036: Blog listing @vp
* Task #24699851: Default page styling @vp
* Task #24893825: Opportunities section update @vp
* Task #24699841: Articles section @vp
* Task #24699835: Info section @vp
* Task #24699838: Testimonials section @vp


v0.2.0.1[stable]

----------

* Task #24699662: Header and main navigation @ak
* Task #24699678: Hero section @ak
* Task #24699733: User flow section @ak
* Task #24699738: Features section @ak
* Task #24699829: How it works @ak
* Task #24699833: Form section @ak
* Task #24699834: Opportunities section @ak
* Task #24699840: Cards section @ak
* Task #24699849: Footer @ak


v0.1.0.1

----------

* Task #24684377: Environment setup @ak



=============================
