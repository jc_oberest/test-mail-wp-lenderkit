<?php
/**
 * This script prepare composer.json to be run inside server-dev installation.
 */

$rootPath = __DIR__;
$lkPath = "{$rootPath}/lenderkit";
$mapping = [
    'search'  => [
        'wp-theme-',
        'wp-plugin-',
    ],
    'replace' => [
        'themes/',
        'plugins/lk-',
    ],
];

function info($msg, $offset = 0)
{
    echo str_repeat("    ", $offset) . $msg . "\n";
}

info('Parsing composer.json and replace repositories for server-dev installation...');

$composer = json_decode(file_get_contents("{$rootPath}/composer.json"), true);

// replace directly connected repos, remove custom composer repo.
$directPackages = [];
foreach ($composer['repositories'] as $key => $repo) {
    if (empty($repo['url'])) {
        continue;
    }

    $repoUrl = $repo['url'];

    // remove custom composer repository (by IP for local test and by prod url).
    if ('composer' === $repo['type']
        && (preg_match('#//[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}#', $repoUrl)
            || false !== strpos($repoUrl, 'license.lenderkit.com')
        )
    ) {
        info("removing composer repository: {$repoUrl}", 1);
        unset($composer['repositories'][$key]);
        continue;
    }

    // replace urls.
    if (preg_match('#(//.+?/lenderkit-dev(/|-)([^.]+?)(-src)?\.git)$#i', $repoUrl, $match)) {
        $replace_path = str_replace($mapping['search'], $mapping['replace'], $match[3]);
        $directPackages[] = $replace_path;
        info("replacing repo with path {$match[1]}", 1);

        $composer['repositories'][$key] = [
            'type' => 'path',
            'url'  => 'lenderkit/' . $replace_path,
        ];
    }
}

// replace lenderkit package versions to any.
info('replacing lenderkit package versions', 1);
foreach ($composer['require'] as $package => $version) {
    if (preg_match('#^lenderkit-wp/#', $package)) {
        $composer['require'][$package] = '*';
    }
}

// reset keys.
$composer['repositories'] = array_values($composer['repositories']);

// encode.
$newComposer = json_encode($composer, JSON_PRETTY_PRINT);

if (! file_put_contents($lkComposerPath = "{$lkPath}/composer.json", $newComposer)) {
    throw new RuntimeException("Unable to write new composer.json file into {$lkComposerPath}");
}

info("Composer generated: {$lkComposerPath}");
