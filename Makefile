.PHONY: info init init-dev build run test-run install update stop xdebug-init chown npm-i npm-build npm-watch php-bash php-exec nodejs-bash mysql-grants
.ONESHELL:

info:
	@echo "WorpdPress Server Configuration/Launcher"
	@echo " "
	@echo "Usage:"
	@echo "	make command [V=1]"
	@echo " "
	@echo "Options:"
	@echo "	V=1 			Vagrant mode, use it in Vagrant to prevent permission errors"
	@echo "	NONE_INTERACTIVE=1	Use it in CI to prevent errors with docker-compose exec\run"
	@echo " "
	@echo "Available commands:"
	@echo "	init-dev 		Init configurations"
	@echo "	build	 		Build images"
	@echo "	test-run		Docker run in place"
	@echo "	run	 		Docker run as daemon"
	@echo "	stop			Docker stop"
	@echo "	install	 		Run install process (php and nodejs)"
	@echo "	update	 		Run install process (php and nodejs)"
	@echo "	test	 		Run all necessary project tests on running containers"
	@echo "	mysql-grants		Add GRANT OPTION to docker mysql container lenderkit user"
	@echo "	chown	 		Return back correct file owner for files created inside a container"
	@echo "	php-bash		Open php-fpm container bash"
	@echo "	php-exec		Execute bash command inside app php-fpm container"

CURDIR_BASENAME = $(notdir ${CURDIR})

SHELL = /bin/bash
DOTENV = $(shell cat .env )
DB_DATABASE = $(shell cat .env | grep "^DB_DATABASE=" | sed 's/DB_DATABASE=//')
DB_USERNAME = $(shell cat .env | grep "^DB_USERNAME=" | sed 's/DB_USERNAME=//')
DB_PASSWORD = $(shell cat .env | grep "^DB_PASSWORD=" | sed 's/DB_PASSWORD=//')
DB_DOCKER_CONTAINER = $(shell [[ `docker-compose images` =~ "mysql" ]] && echo '1' || echo '0')

INSTALLATION_MODE = project
DOCKER_COMPOSE_OVERRIDE_SRC = build/docker-compose.dev.yml

ifeq "$(CURDIR_BASENAME)" "boilerplate"
	# mount directory with core and modules exists, adjust composer.json
	INSTALLATION_MODE = dev
	DOCKER_COMPOSE_OVERRIDE_SRC = build/docker-compose.serverdev.yml
endif

# check if we running a vagrant
ifeq "$(USER)" "vagrant"
	V = 1
endif

VAGRANT =
MAYBE_SUDO =
ifneq "$(V)" ""
	VAGRANT = 1
	MAYBE_SUDO = sudo
endif


DOCKER_T_FLAG =
ifeq "$(NON_INTERACTIVE)" "1"
	DOCKER_T_FLAG = -T
endif

DOCKER_COMPOSE_EXEC = docker-compose exec ${DOCKER_T_FLAG} --privileged --index=1
DOCKER_COMPOSE_EXEC_WWW = ${DOCKER_COMPOSE_EXEC} -w /var/www/html
DOCKER_COMPOSE_RUN = docker-compose run ${DOCKER_T_FLAG} -w /var/www/html

############################################
# Make Targets
############################################

init:
	@if [ ! -f '.env' ]; then \
		echo 'Copying .env file...'; \
		${MAYBE_SUDO} cp .env.example .env; \
	fi
	@if [ ! -f 'docker-compose.yml' ]; then \
		echo 'Copying docker-compose.yml file...'; \
		${MAYBE_SUDO} cp docker-compose.example.yml docker-compose.yml; \
	fi
	@if [ ! -f 'configs/nginx-server.conf' ]; then \
		echo 'Copying nginx config file...'; \
		${MAYBE_SUDO} cp ./configs/nginx-server.example.conf ./configs/nginx-server.conf; \
	fi
	@echo ''
	@echo 'NOTE: Please check your configuration in ".env" before run.'
	@echo 'NOTE: Please check your configuration in "docker-compose.yml" before run.'
	@echo 'NOTE: You can update nginx server configuration in "configs/nginx-server.conf".'
	@echo ''

re-init:
	${MAYBE_SUDO} cp -f .env.example .env
	${MAYBE_SUDO} cp -f docker-compose.example.yml docker-compose.yml
	${MAYBE_SUDO} cp -f ./configs/nginx-server.example.conf ./configs/nginx-server.conf

init-dev: init
	@if [ ! -f 'docker-compose.override.yml' ]; then \
		echo 'Copying "docker-compose.override.yml" with dev mode envs...'; \
		${MAYBE_SUDO} cp ${DOCKER_COMPOSE_OVERRIDE_SRC} docker-compose.override.yml; \
	fi

install:
	set -e
	docker-compose stop
	${DOCKER_COMPOSE_RUN} wp-app bash -c "make install SKIP_VENDORS=${SKIP_VENDORS}"
	${DOCKER_COMPOSE_RUN} wp-app bash -c "make chmod && make chown"
	$(MAKE) npm-i
	$(MAKE) npm-build
	${MAYBE_SUDO} mkdir runtime || true
	${MAYBE_SUDO} touch runtime/installed
	$(MAKE) run V=${VAGRANT}

update:
	set -e
	docker-compose stop
	${DOCKER_COMPOSE_RUN} wp-app bash -c "make update SKIP_VENDORS=${SKIP_VENDORS}"
	${DOCKER_COMPOSE_RUN} wp-app bash -c "make chmod && make chown"
	@if [ -z '$(SKIP_VENDORS)' ]; then \
		$(MAKE) npm-i; \
		$(MAKE) npm-build; \
	else \
		echo 'SKIP_VENDORS is active. Skipping npm install and npm build...'; \
		${DOCKER_COMPOSE_RUN} wp-app bash -c "make chmod && make chown"; \
	fi
	$(MAKE) npm-i
	$(MAKE) npm-build
	$(MAKE) run V=${VAGRANT}

build:
	docker-compose build

run: xdebug-init
	docker-compose up --force-recreate --scale wp-nodejs=0 -d

test-run: xdebug-init
	docker-compose up --force-recreate --scale wp-nodejs=0

xdebug-init:
	@if [ ! -z '$(VAGRANT)' ]; then \
		export XDEBUG_REMOTE_HOST=`/sbin/ip route|awk '/default/ { print $$3 }'` \
			&& echo "Set XDEBUG_REMOTE_HOST to $${XDEBUG_REMOTE_HOST} in .env" \
			&& sudo sed -i "s/XDEBUG_REMOTE_HOST=.*/XDEBUG_REMOTE_HOST=$${XDEBUG_REMOTE_HOST}/g" .env; \
	fi

test:
	echo "test"

logs:
	docker-compose logs -f

stop:
	docker-compose down

chown:
	sudo chown -R $$(whoami) .env .env.* docker-compose.* src/ configs/

php-bash:
	${DOCKER_COMPOSE_EXEC_WWW} wp-app bash

# TODO: add general command runner inside docker by container name and cmd.
php-exec:
	@if [ ! -z '$(COM)' ]; then \
		echo "${DOCKER_COMPOSE_EXEC_WWW} wp-app bash -c '${COM}'"; \
		${DOCKER_COMPOSE_EXEC_WWW} app bash -c '${COM}'; \
	else \
		echo "COM directive should be set. Example: make php-exec COM='make chmod'"; \
	fi

NODEJS_OPTS = export NODE_OPTIONS=--max_old_space_size=$(NODEJS_MEMORY_LIMIT)
NODEJS_BASH_CMD = docker-compose run -w /var/www/html/ wp-nodejs bash
NODEJS_WORK_DIR = cd /var/www/html/ &&

nodejs-bash:
	$(NODEJS_BASH_CMD)

npm-i:
	$(NODEJS_BASH_CMD) -c 'cd wp-content/themes/$$LK_THEME/assets && npm install'

npm-install:
	$(NODEJS_BASH_CMD) -c 'cd wp-content/themes/$$LK_THEME/assets && npm install'

npm-build:
	$(NODEJS_BASH_CMD) -c '$(NODEJS_OPTS) && cd wp-content/themes/$$LK_THEME/assets && gulp build'

npm-watch:
	$(NODEJS_BASH_CMD) -c '$(NODEJS_OPTS) && cd wp-content/themes/$$LK_THEME/assets && npm run watch-poll'

init-watch:
	# increase system watchers amount
	echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
